# README #

This README would normally document whatever steps are necessary to get your application up and running.

### Summary ###
* Project name: `N/A`
* Version: `N/A`

### Checklist ###
- [ ] Min-SDK version: 10
- [ ] Target-SDK version: 21
- [ ] Build Tools Version: 21
- [ ] Support devices(phone/tablet): N/A
- [ ] App package name: N/A
- [ ] API debug url: N/A
- [ ] API release url: N/A
- [ ] Release keystore: N/A
- [ ] Facebook ID(option): N/A

### Environment & Library ###
* Android studio v1.3.2
* AndroidAnnotations v3.3.2
* Android Saripaar v2.0.2
* Lombok v1.16.4
* universalimageloader v1.9.3
* Gson v2.3.1
* retrofit v1.9.0
* otto v1.3.7
* Junit 3.0
* robolectric 3.0

### Guidelines ###

* [Coding conventions](http://wiki.asiantech.vn:8080/documents/index.php/Android:coding_convention)
* [Github flow](http://wiki.asiantech.vn:8080/documents/index.php/Android:quy_%C4%91%E1%BB%8Bnh_s%E1%BB%AD_d%E1%BB%A5ng_github)