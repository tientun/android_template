package vn.asiantech.baseproject.network;

/**
 * APIパラメータの定数クラス
 * 敢えてアクセス修飾子を無しにしてます。
 */
public class Parameter {

    static final String EMAIL = "email";
    static final String PASSWORD = "password";
}
