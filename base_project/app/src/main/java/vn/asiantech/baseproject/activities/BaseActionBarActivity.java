package vn.asiantech.baseproject.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import org.androidannotations.annotations.EActivity;

/**
 * Created by tientun on 3/5/15.
 */
@EActivity
public abstract class BaseActionBarActivity extends AppCompatActivity {
    protected String tag = this.getClass().getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }
}
