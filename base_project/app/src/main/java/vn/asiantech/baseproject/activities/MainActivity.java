package vn.asiantech.baseproject.activities;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.os.Build;

import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.OptionsItem;

import vn.asiantech.baseproject.R;

/**
 * Created by tientun on 3/5/15.
 */

@SuppressLint("Registered")
@EActivity(R.layout.activity_main)
public class MainActivity extends BaseActionBarActivity {

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    @OptionsItem(android.R.id.home)
    protected void backAction() {
        finish();
    }

}
