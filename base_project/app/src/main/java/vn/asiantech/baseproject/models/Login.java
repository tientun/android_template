package vn.asiantech.baseproject.models;

import lombok.Data;

/**
 * Copyright © 2015 AsianTech inc.
 * Created by tientun on 7/8/15.
 */
@Data
public class Login {
    private String user_id;
    private String email;

    private String token;
}
