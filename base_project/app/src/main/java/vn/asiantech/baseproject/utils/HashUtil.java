package vn.asiantech.baseproject.utils;

import android.util.Log;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * Copyright © 2015 AsianTech inc.
 * Created by TienHN on 3/10/15.
 */
public final class HashUtil {

    private HashUtil() {

    }

    /**
     * Encrypt string to MD5
     *
     * @param text input string
     * @return encrypted string
     */
    public static String md5(String text) {
        MessageDigest digest;
        try {
            digest = MessageDigest.getInstance("MD5");
            digest.reset();
            digest.update(text.getBytes());
            byte[] arr = digest.digest();
            StringBuilder sb = new StringBuilder(arr.length << 1);
            for (byte a : arr) {
                sb.append(Character.forDigit((a & 0xf0) >> 4, 16));
                sb.append(Character.forDigit(a & 0x0f, 16));
            }
            return sb.toString();
        } catch (NoSuchAlgorithmException e) {
            Log.d("---", e.getMessage());
        }
        return null;
    }
}
