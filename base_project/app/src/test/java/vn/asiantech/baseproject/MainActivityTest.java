package vn.asiantech.baseproject;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.annotation.Config;

import vn.asiantech.baseproject.base.MyRobolectricTestRunner;

import static org.junit.Assert.assertTrue;

/**
 * Copyright © 2015 AsianTech inc.
 * Created by tienhn on 7/27/15.
 */
@RunWith(MyRobolectricTestRunner.class)
@Config(constants = BuildConfig.class)
public class MainActivityTest {

    @Test
    public void titleIsCorrect() throws Exception {
        assertTrue(true);
    }
}
