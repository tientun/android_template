package vn.asiantech.baseproject;

import com.google.gson.Gson;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.annotation.Config;

import vn.asiantech.baseproject.base.BaseTest;
import vn.asiantech.baseproject.base.MyRobolectricTestRunner;
import vn.asiantech.baseproject.models.Login;


/**
 * Copyright © 2015 AsianTech inc.
 * Created by tienhn on 7/27/15.
 */

@RunWith(MyRobolectricTestRunner.class)
@Config(constants = BuildConfig.class)
public class ParseSampleTest extends BaseTest {
    private static final String FILE_NAME = "SampleUnitTestSample.json";
    private Login data;

    @Before
    public void setup() throws Exception {
        this.data = new Gson().fromJson(readAssetsSampleFile(FILE_NAME), Login.class);
    }

    @Test
    public void testMetaData() throws Exception {
//        Assert.assertEquals("successfully", data.getMeta().getStatus());
    }

    @Test
    public void testFullData() throws Exception {
//        Assert.assertEquals(123, data.getData().getUser().getId());
    }
}
